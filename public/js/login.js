var $tab = $(".login-way a"), wait = 60, errorMsg, flag;

$(function() {
    $("[data-toggle='tooltip']").tooltip();

    $tab.click(function() {
        $(this).removeClass("text-muted").siblings().addClass("text-muted");
        var index = $tab.index(this);
        if (index == 1) {
            codeLogin();
        } 
        else {
            fastLogin();
        }
    });

    $('form :input').blur(function() {
        var $parent = $(this).parent();
        // $parent.find(".error-tip").remove();

        //验证手机号
        if ($(this).is('#phoneNumber')) {
            if (this.value === "" || this.value.length < 11) {
                errorMsg = '请输入11位手机号';
                $parent.append('<label class="error-tip">' + errorMsg + '</label>');
            } else if((this.value !== "" && !/^1[3|4|5|7|8][0-9]\d{8}$/.test(this.value))){
                errorMsg = '手机号格式错误';
                $parent.append('<label class="error-tip">' + errorMsg + '</label>');
            } else {
                $parent.append('');
            }
        }

        //验证密码
        if ($(this).is('#password')) {
            if (this.value === "") {
                errorMsg = '请输入密码';
                $parent.append('<label class="error-tip">' + errorMsg + '</label>');
            }
        }

        //验证确认密码
        if ($(this).is('#rePassword')) {
            if (this.value === "") {
                errorMsg = '请再次输入密码';
                $parent.append('<label class="error-tip">' + errorMsg + '</label>');
            } 
            if (this.value !== "" && this.value !== document.getElementById('password').value) {
                errorMsg = '两次输入的密码不一致';
                $parent.append('<label class="error-tip">' + errorMsg + '</label>');
            }
        }

        //验证图片验证码
        if ($(this).is('#veriCode')) {
            if (this.value === "") {
                errorMsg = '请输入图片验证码';
                $parent.append('<label class="error-tip error-code">' + errorMsg + '</label>');
            }
        }

        //验证短信验证码
        if ($(this).is('#smsCode')) {
            if (this.value === "") {
                errorMsg = '请输入短信验证码';
                $parent.append('<label class="error-tip error-code">' + errorMsg + '</label>');
            }
        }
    })

    $("#login-btn").click(function() {
        checkSubmit();
    });

    //绑定回车键
    document.onkeydown = function(event) {
        var e = event ? event : (window.event ? window.event : null);
        if (e.keyCode == 13) {
            checkSubmit();
        }
    };

    //获取短信验证码
    $('#obtain-code').click(function() {
        var $tel = $("#phoneNumber").val(), //获取手机号
            $psd = $("#password").val(),    //获取密码
            $rsd = $("#rePassword").val(),  //获取确认密码
            $sms = $("#smsCode").val(),     //获取短信验证码
            $img = $("#veriCode").val();    //获取图片验证码

        if ($tel === "" || $tel.length < 11) {
            errorMsg = '请输入11位手机号';
            $('.phone-num').append('<label class="error-tip">' + errorMsg + '</label>');
        } else if(($tel !== "" && !/^1[3|4|5|7|8][0-9]\d{8}$/.test($tel))){
            errorMsg = '手机号格式错误';
            $('.phone-num').append('<label class="error-tip">' + errorMsg + '</label>');
        } else {
            $('.phone-num').append('');
        }

        if ($img === "") {
            errorMsg = '请输入图片验证码';
            $('.veri-code').append('<label class="error-tip error-code">' + errorMsg + '</label>');
            return false;
        }

        getTime(this);
    });
});


function checkSubmit() {
    var $tel = $("#phoneNumber").val(), //获取手机号
        $psd = $("#password").val(),    //获取密码
        $img = $("#veriCode").val(),    //获取图片验证码
        $sms = $("#smsCode").val();     //获取短信验证码

    $("form :input").trigger('blur');
    var numError = $('form .error-tip').length;
    if (numError) {
        return false;
    }
    if (flag === 1) {
        if ($img === "") {
            errorMsg = "请输入图片验证码";
            $(".veri-code").append('<label class="error-tip">' + errorMsg + '</label>');
        }
        if ($sms === "") {
            errorMsg = '请输入短信验证码';
            $(".sms-code").append('<label class="error-tip">' + errorMsg + '</label>');
        }
    } else{
        if ($psd === "") {
            errorMsg = '请输入密码';
            $("password").append('<label class="error-tip">' + errorMsg + '</label>');
        }
    };
    $.ajax({
        type: 'GET',
        url: "/loginS",
        data: {
            username: $tel,
            password: $psd
        },
        success: function(data) {
            console.log(data);
            if (data.result === 'ok') {
                setTimeout(function() {location.href = "/"}, 1000);}
            else {
                $('#error').text(data.message);
            }
        }
    });
}

//短信验证码60秒倒计时
function getTime(o) {
    if (wait === 0) {
        o.removeAttribute("disabled");
        o.innerHTML = "获取验证码";
        wait = 60;
    } else {
        o.setAttribute("disabled", true);
        o.innerHTML = "重新发送(" + wait + ")";
        wait--;
        setTimeout(function() {getTime(o);}, 1000);
    }
}

function codeLogin() {
    flag = 1;
    $('#error').text("");
    $("#phoneNumber, #veriCode, #smsCode").val("");
    $(".veri-code, .sms-code").show();
    $(".password").hide();
}

function fastLogin() {
    flag = 2;
    $('#error').text("");
    $("#phoneNumber, #password").val("");
    $(".veri-code, .sms-code").hide();
    $(".password").show();
}
