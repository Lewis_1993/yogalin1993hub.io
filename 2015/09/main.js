var ANIMATE_EVENTS = {
    animateEnd: 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
    transitionEnd: 'transitionend webkitTransitionEnd MSTransitionEnd'
}

$('.i-green').click(function() {

    var currElem = $('#get'),
        iconElem = currElem.find('.i-green');

    var ring = document.getElementById('ring');
    ring.pause();

    currElem.find('span').hide();
    self.$('.act-btn-left').hide();

    iconElem.addClass('transform-rotate').on(ANIMATE_EVENTS.transitionEnd, function() {
        // step1 隐藏该隐藏的内容
        self.$('.act-wrap-btn, .act-wrap-ctx, #cellphone').addClass('animate-fade-out');
        // step2 监听隐藏完毕时间点 彻底隐藏该隐藏内容 展现要展现的内容
        self.$('.act-wrap-btn').on(ANIMATE_EVENTS.animateEnd, function() {
            self.$('.act-star img').hide();
            self.$('.act-wrap-btn').hide();
            self.$('.act-wrap-ctx').hide();
            self.$('#cellphone').hide();
            // .act-star img 使用超哥头像把titleName拖上去
            self.$('#count').show();
            self.$('.act-star-answer')
                .addClass('act-title-hotfix').addClass('animate-fade-out-in')
                .on(ANIMATE_EVENTS.animateEnd, function() {
                    self.$('.act-wrap-solo').show().addClass('animate-fade-in');
                    self.$('.act-wrap-end').show().addClass('animate-fade-in');
                    // 计时器
                    self.initAudio(self.openCount);
                });
        });

        iconElem.removeClass('transform-rotate').addClass('transform-rotate-reset');
        currElem.addClass('animate-fade-out-fly-r').on(ANIMATE_EVENTS.animateEnd, function() {
            currElem.removeClass('animate-fade-out-fly-r');
            iconElem.hide();
        });
    });
})

$('.i-red').click(function() {
    var currElem = $('#refuse'),
        iconElem = currElem.find('.i-red');

    if (currElem.size() < 1) return;

    $.each(self.$('#ring'), function(i, ring) {
        ring.pause();
    });

    currElem.find('span').hide();
    self.$('.act-btn-right').hide();

    iconElem.addClass('transform-rotate').on(ANIMATE_EVENTS.transitionEnd, function() {
        iconElem.removeClass('transform-rotate').addClass('transform-rotate-reset');
        currElem.addClass('animate-fade-out-fly-l').on(ANIMATE_EVENTS.animateEnd, function() {
            currElem.removeClass('animate-fade-out-fly-l');
            iconElem.hide();
            // 页面跳转 到chat页
            window.location.assign('./chat.html');
        });
    });
});

$('.i-red-big').click(function() {
    var currElem = $('#pause');

    $.each(self.$('#audio'), function(i, o) {
        o.pause();
    });

    clearInterval(self.timer);

    currElem && currElem.addClass('animate-fade-out').on(ANIMATE_EVENTS.animateEnd, function() {
        currElem.removeClass('animate-fade-out').hide();
        window.location.assign('./chat.html');
    });
});

function initAudio(callback) {

    $.each(self.$('#audio'), function(i, o) {
        o.play();
    });

    typeof callback === 'function' && callback.call(self);
    // audio.on
    audio.addEventListener('ended', function() {

        clearInterval(self.timer);
        // 这里跳转 到国庆红包
        window.location.assign('./activity.html');

    }, false);
}

function openCount() {

    var startD = new Date(),
        startD_Min = startD.getMinutes(),
        startD_Sec = startD.getSeconds();

    clearInterval(self.timer);

    self.timer = setInterval(function() {

        var currD = new Date(),
            currD_Min = currD.getMinutes(),
            currD_Sec = currD.getSeconds(),
            sec = 0;

        currD_Sec = currD_Sec < startD_Sec ? currD_Sec + 60 : currD_Sec;
        sec = currD_Sec - startD_Sec;
        sec = sec < 10 ? '0' + sec : sec;

        self.$('#count').text('00:' + sec);

    }, 300);
}